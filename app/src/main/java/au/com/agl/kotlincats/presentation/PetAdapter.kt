package au.com.agl.kotlincats.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.agl.kotlincats.R

class PetAdapter(private var context: Context?, private val items: List<AdapterItem<String>>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class AdapterItem<T> internal constructor(var viewType: Int, var item: T)

    companion object {
        const val VIEW_TYPE_GENDER = 0
        const val VIEW_TYPE_PET_NAME= 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            VIEW_TYPE_GENDER -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
                ItemViewHolder(itemView)
            }
            VIEW_TYPE_PET_NAME -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_value, parent, false)
                ItemViewHolder(itemView)
            }
            else -> throw Exception("Unknown item type")
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            (holder as ItemViewHolder).dateTextView.setText(items.get(position).item)
    }

    internal inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var dateTextView: TextView = view.findViewById(R.id.text_view)
    }

    override fun getItemViewType(position: Int): Int {
        return items.get(position).viewType
    }
}