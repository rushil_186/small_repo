package au.com.agl.kotlincats.domain

import au.com.agl.kotlincats.common.Callback
import au.com.agl.kotlincats.data.OwnerRepository
import au.com.agl.kotlincats.data.model.Owner
import au.com.agl.kotlincats.presentation.PetAdapter
import kotlin.collections.ArrayList

class MainUseCases(private val ownerRepository: OwnerRepository): MainFacade {
    override fun loadGroupedCats(callback: Callback<Any>) {
        ownerRepository.get(object: Callback<List<Owner>> {
            override fun onSuccess(data: List<Owner>) {
                val groupedData = groupAccordingToGender(data)
                val adapterItems = generateAdapterItems(groupedData)
                callback.onSuccess(adapterItems)
            }

            override fun onError(error: Throwable) {
                callback.onError(error)
            }
        })
    }


    data class GenderGroup(val gender: String, val pets: List<String>?)


    /**
     * Returns a list containing the GenderGroup after transforming raw feed into meanigful data
     * @return List<GenderGroup>
     *
     */

     fun groupAccordingToGender(data: List<Owner>) :List<GenderGroup>{
        //group by gender and extract pet name
        val grouped = data.groupBy({it.gender},{ it.pets?.map { it.name } }).toMutableMap()
       var mutableGrouped =  grouped.map {

           //flatten pet names
           var flattenedListOfPetNames = ArrayList<String>()
           it.value.filterNotNull()
           it.value.forEach{names->
               names?.filterNotNull()
               names?.forEach { name->
                   flattenedListOfPetNames.add(name)
               }
            }
           //sort
           flattenedListOfPetNames.sort()

           GenderGroup(it.key,flattenedListOfPetNames)
       }
        return mutableGrouped
    }


    /**
     * Returns a list containing the items for the adpater
     * @return  PetAdapter.AdapterItem<String>
     *
     */
     fun generateAdapterItems(data: List<GenderGroup>) :List<PetAdapter.AdapterItem<String>>{
        var adapterItems = ArrayList<PetAdapter.AdapterItem<String>>()
        var mutableGrouped =  data.forEach {
            adapterItems.add(PetAdapter.AdapterItem(PetAdapter.VIEW_TYPE_GENDER,it.gender))
            it.pets?.forEach { name->
                adapterItems.add(PetAdapter.AdapterItem(PetAdapter.VIEW_TYPE_PET_NAME,name))
            }
        }
        return adapterItems
    }

}

